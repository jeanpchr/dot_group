<?php
	require_once("Autoload.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>FizzBuzz</title>
	</head>
	<body>
		<h1>FizzBuzz Printer</h1>
		<p>
			Esse é um programa que imprime números de 1 a 100. E que números múltiplos de 3 são impressos como “Fizz” e números múltiplos de 5 são impressos como “Buzz”. Já números múltiplos de ambos 3 e 5 são impressos como “FizzBuzz”. 
		</p>
		<h2>Números</h2>
		<p>
			<?php  
			$numberGenerator = new FizzBuzz();
			echo $numberGenerator->generateIntervalAsString(1,101);
			?>
		</p>
	</body>
</html>