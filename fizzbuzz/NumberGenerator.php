<?php

interface NumberGenerator 
{
	/**
	* Creates a string containing a printed number interval
	* @param $begin The first number of the interval
	* @param $end The last number of the interval, non inclusive
	* @return string The generated string
	*/
	public function generateIntervalAsString($begin, $end);
}

?>