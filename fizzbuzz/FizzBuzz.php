<?php
require_once("Autoload.php");

class FizzBuzz implements NumberGenerator
{
	/**
	* The separator to be printed between each number
	* @var string 
	*/
	protected $numberSeparator;
	
	/**
	* The separator to be used in case a user doesn't define one
	* @var string 
	*/	
	private $defaultNumberSeparator = ", ";


	public function __construct($separator = null) {
		if ($separator == null) {
			$this->numberSeparator = $this->defaultNumberSeparator;
		
		} else {
			$this->numberSeparator = $separator;
		}
	}

	public function generateIntervalAsString($begin, $end) {
		//prints the first number outside the loop so we don't put a separator after the last number
		$intervalString = $this->printNumber($begin);
		
		for ($i = $begin + 1; $i < $end; $i++) {	
			$intervalString .= $this->numberSeparator . $this->printNumber($i);
		}

		return $intervalString;
	}

	private function printNumber($number) {
		$numberIsDivisibleByThree = (($number % 3) == 0);
		$numberIsDivisibleByFive = (($number % 5) == 0);
		$printedNumber;

		if ($numberIsDivisibleByThree && $numberIsDivisibleByFive) {
			$printedNumber = "FizzBuzz";
		
		} elseif ($numberIsDivisibleByThree) {
			$printedNumber = "Fizz";
		
		} elseif ($numberIsDivisibleByFive) {
			$printedNumber = "Buzz";
		
		} else {
			$printedNumber = $number;
		}

		return $printedNumber;
	}
}

?>