<?php 

class DatabaseManager
{
	private static $connections = array(
		'default' => array (
			'dbString' => 'mysql:host=localhost;dbname=test',
			'user' => 'test',
			'pwd' => 'test'
		),
		
		'your_config_here' => array(
			'dbString' => 'your_db_connection_string',
			'user' => 'your_username',
			'pwd' => 'your_pwd'
		)
	);

	
	public static function getConnection($connectionName = null) {
		if($connectionName != null && isset(DatabaseManager::$connections[$connectionName])) {
			$dbString = DatabaseManager::$connections[$connectionName]['dbString'];
			$user = DatabaseManager::$connections[$connectionName]['user'];
			$pwd = DatabaseManager::$connections[$connectionName]['pwd'];
		
		} else {
			$dbString = DatabaseManager::$connections['default']['dbString'];
			$user = DatabaseManager::$connections['default']['user'];
			$pwd = DatabaseManager::$connections['default']['pwd'];	
		}

		return new PDO($dbString, $user, $pwd, array(PDO::ATTR_PERSISTENT => true));
	}

}

?>