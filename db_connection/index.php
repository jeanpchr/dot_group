<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Banco de Dados</title>
	</head>
	<body>
		<h1>Conexão com o Banco de Dados</h1>
		
		<p>
			Essa página exibe duas amostras de código, ambas feitas para lidar com uma conexão com banco de dados
			e realizar uma consulta simples.
		</p>

		<h2>Código Original</h2>

		<p>
			A primeira amostra de código (a original) é a seguinte:
		</p>
	
	<pre>
	<code>
	//Arquivo: MyUserClass.php
	
	//Código original
	class MyUserClass
	{
		public function getUserList() { 
			$dbconn = new DatabaseConnection('localhost','user','password');
			$results = $dbconn->query('select name from user');
			sort($results);
			return $results;
		}
	}
	</code>
	</pre>

		<p>
			O problema que vejo nessa classe é o fato dos detalhes da conexão com o banco estarem sendo especificados
			diretamente em uma classe de modelo de dados. Isso é um problema pois se esse estilo de código for adotado
			em todo o sistema, todas as classes de modelo vão ter desnecessariamente informações sobre o banco de dados
			(replicação de código desnecessária). Além do que caso alguma das informações referente ao banco seja 
			alterada, fica muito trabalhoso refatorar todo os modelos.			
		</p>

		<p>
			Outra coisa da qual não sou muito fã é inserir a lógica de consulta diretamente no modelo de dados. Não vejo 
			problema em utilizar a classe para acessar os dados do banco, porém se possível prefiro separar queries em uma 
			classe a parte (o famoso DAO).
		</p>

		<h2>Código Refatorado</h2>

	<pre>
	<code>
	//Arquivo: MyUserDao.php
	
	require_once('Autoload.php');

	class MyUserDao 
	{
		public function getUserList() { 
			$dbconn = DatabaseManager::getConnection();
			$results = $dbconn->query('select * from user order by name asc')->fetchAll(PDO::FETCH_ASSOC);
			$users = array();
			foreach ($results as $userData) {
				$users[] = new MyUser($userData['name'], (int) $userData['id']);
			}
			
			return $users;
		}	
	}

	//Arquivo DatabaseManager.php

	class DatabaseManager
	{
		private static $connections = array(
			'default' => array (
				'dbString' => 'mysql:host=localhost;dbname=test',
				'user' => 'test',
				'pwd' => 'test'
			),
			
			'your_config_here' => array(
				'dbString' => 'your_db_connection_string',
				'user' => 'your_username',
				'pwd' => 'your_pwd'
			)
		);

		
		public static function getConnection($connectionName = null) {
			if($connectionName != null && isset(DatabaseManager::$connections[$connectionName])) {
				$dbString = DatabaseManager::$connections[$connectionName]['dbString'];
				$user = DatabaseManager::$connections[$connectionName]['user'];
				$pwd = DatabaseManager::$connections[$connectionName]['pwd'];
			
			} else {
				$dbString = DatabaseManager::$connections['default']['dbString'];
				$user = DatabaseManager::$connections['default']['user'];
				$pwd = DatabaseManager::$connections['default']['pwd'];	
			}

			return new PDO($dbString, $user, $pwd, array(PDO::ATTR_PERSISTENT => true));
		}

	}

	//Arquivo MyUser.php

	class MyUser
	{
		private $id;
		private $name;

		public function __construct($name, $id = null) {
			$this->id = $id;
			$this->name = $name;
		}

		public function getId() {
			return $this->id;
		}

		public function getName() {
			return $this->name;
		}

		public function setName($name) {
			$this->name = $name;	
		}	
	}
	</code>
	</pre>

		<p>
			No código refatorado eu separei as regras para realizar a conexão com o banco na classe DatabaseManager,
			as queries na classe MyUserDao e o modelo de dados na classe MyUser.
		</p>

		<p>
			A classe DatabaseManager fica responsável por retornar uma conexão com o banco (Objeto PDO) quando solicitada.
			Essa classe também permite que o usuário configure mais de uma conexão com bancos de dados, a qual pode ser escolhida
			especificando seu nome no método "getConnection". Para que a conexão funcione no entanto, ela precisa estar 
			registrada no atributo "connections" e precisa ser setada diretamente no arquivo.
		</p>

		<p>
			A classe MyUserDao serve como contâiner para as queries referente a classe MyUser
		</p>

		<p>
			A classe MyUser é o modelo de dados referente a tabela do banco "user". 
		</p>
	
	</body>
</html>