<?php 

require_once('Autoload.php');

class MyUserDao 
{
	public function getUserList() { 
		$dbconn = DatabaseManager::getConnection();
		$results = $dbconn->query('select * from user order by name asc')->fetchAll(PDO::FETCH_ASSOC);
		$users = array();
		foreach ($results as $userData) {
			$users[] = new MyUser($userData['name'], (int) $userData['id']);
		}
		
		return $users;
	}	
}

?>