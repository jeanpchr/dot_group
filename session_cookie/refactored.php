<?php

require_once("SessionUtils.php");

if(SessionUtils::isLoggedin()) {
	header("Location: http://www.google.com");
	exit();
}

?>