<?php

class SessionUtils
{
	public static function isLoggedin() {
		return isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true;
	}

	public static function login($user) {
		$_SESSION['loggedin'] = true;
		//Possibly sets some other session variables
	}
}

?>