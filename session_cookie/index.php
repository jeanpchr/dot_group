<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Cookies e Sessão</title>
	</head>
	<body>
		<h1>Uso de Cookies e Sessão para Login</h1>
		
		<p>
			Essa página exibe duas amostras de código, ambas feitas para lidar com uma variável "loggedin" 
			que serve para identificar se um usuário está logado ou não.
		</p>
		
		<h2>Código Original</h2>

		<p>
			A primeira amostra de código (a original) é a seguinte:
		</p>

	<pre>
	<code>
	//Arquivo: original.php

	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) { 
		header("Location: http://www.google.com"); 
		exit();

	} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) { 
		header("Location: http://www.google.com");
		exit();
	}
	</code>
	</pre>

		<p>
			Nela para determinar se um usuário está logado ou não no sistema, o script verifica se existe
			no array da sessão ($_SESSION) o índice "loggedin" e se caso ele exista, é verificado se o seu valor é 
			igual a "true".	Caso não haja o registro na sessão ou seu valor seja falso ele prossegue para a segundo caso,
			onde ele (o script) verifica se existe no array de cookies ($_COOKIE) o índice "Loggedin". Existindo
			o índice "Loggedin" o script verifica se o valor o qual ele indexa é "true".
		</p>

		<p>
			Existe um grande problema de segurança nessa abordagem que verifica se um usuário está logado ou não, 
			precisamente o trecho "elseif" que verifica o cookie. A verificação através do cookie permite que qualquer 
			pessoa utilizando o sistema, crie em seu navegador o cookie "Loggedin" com valor "true" e ganhe 
			automaticamente acesso a funcionalidades restritas a usuários logados.			
		</p>

		<p>
			Além do problema de segurança há espaço para melhorias no design do código, pois a checagem de usuários logados
			é algo rotineiro dentro de qualquer sistema, sendo assim, uma funcionalidade a qual deveria ser encapsulada em 
			uma classe para impedir a duplicação de código.
		</p>
		
		<h2>Código Refatorado</h2>

	<pre>
	<code>
	//Arquivo: SessionUtils.php

	class SessionUtils
	{
		public static function isLoggedin() {
			return isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true;
		}

		public static function login($user) {
			$_SESSION['loggedin'] = true;
			//Possibly sets some other session variables
		}
	}

	//Arquivo: refactored.php

	require_once("SessionUtils.php");

	if(SessionUtils::isLoggedin()) {
		header("Location: http://www.google.com");
		exit();
	}
	</code>
	</pre>

		<p>
			No código refatorado a parte de verificação com cookies é eliminada (removendo o problema de segurança)
			e é criada uma classe SessionUtils para lidar com problemas referentes a sessão.
		</p>

		<p>
			Há a possibilidade para permitir o uso de cookies. A solução seria um script que "embaralhasse" alguns dados
			do usuário e os armazenasse em cookies, os quais permitiriam o usuário a "permanecer logado" mesmo após o fim da sua sessão. 
		</p>

		<p>
			Implementaria essa solução da seguinte forma:	
		</p>

	<pre>
	<code>
	//Arquivo: SessionUtilsV2.php

	class SessionUtilsV2
	{
		public static function isLoggedin() {
			if(SessionUtilsV2::checkCookieCredentials()) {
				session_start();
				$_SESSION['loggedin'] = true;
			}

			return isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true;
		}

		public static function login($user) {
			$_SESSION['loggedin'] = true;
			SessionUtilsV2::setCookieCredentials($user);
			//Possibly sets some other session variables
		}

		/**
		* Verifies if the current user has a cookie with scrambled credential data
		* and if this data represents a valid system user
		*
		* @return bool Wheter or not the current user had valid scrambled credentials data
		* 		  in their cookies	
		*/
		private static function checkCookieCredentials() {
			//Code Here
			//I have no acess to the user model, no way of implementing this at the moment
		}

		/**
		* Creates a cookie storing harmless and scrambled user information that can be later
		* used to verify wheter or not a returning user is a valid system user that can be
		* automatically logged in
		*/
		private static function setCookieCredentials($user){
			//Code Here
			//I have no acess to the user model, no way of implementing this at the moment
		}
	}
	</code>
	</pre>

	<p>
	
	</p>

	</body>
</html>