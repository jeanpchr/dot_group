<?php

class SessionUtilsV2
{
	public static function isLoggedin() {
		if(SessionUtilsV2::checkCookieCredentials()) {
			session_start();
			$_SESSION['loggedin'] = true;
		}

		return isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true;
	}

	public static function login($user) {
		$_SESSION['loggedin'] = true;
		SessionUtilsV2::setCookieCredentials($user);
		//Possibly sets some other session variables
	}

	/**
	* Verifies if the current user has a cookie with scrambled credential data
	* and if this data represents a valid system user
	*
	* @return bool Wheter or not the current user had valid scrambled credentials data
	* 		  in their cookies	
	*/
	private static function checkCookieCredentials() {
		//Code Here
		//I have no acess to the user model, no way of implementing this at the moment
	}

	/**
	* Creates a cookie storing harmless and scrambled user information that can be later
	* used to verify wheter or not a returning user is a valid system user that can be
	* automatically logged in
	*/
	private static function setCookieCredentials($user){
		//Code Here
		//I have no acess to the user model, no way of implementing this at the moment
	}
}

?>