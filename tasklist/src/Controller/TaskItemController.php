<?php
namespace App\Controller;

use Cake\I18n\Time;

use App\Controller\AppController;

/**
 * TaskItem Controller
 *
 * @property \App\Model\Table\TaskItemTable $TaskItem
 */
class TaskItemController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
    * Lista as tarefas armazenadas no sistema, permite filtrar a consulta por status.
    * Para realizar a filtragem basta especificar na query string o parâmetro "status"
    * com o valor do id do status desejado
    * @return array - array contendo as tarefas resultantes da consulta
    */
    public function index()
    {   

        if(isset($this->request->query['status'])) {
            $taskItem = $this->TaskItem->find('all', array(
                'conditions' => array('id_status' => $this->request->query['status']),
                'order' => array('priority' => 'DESC', 'creation_date' => 'ASC')
            )); 

        }else {
            $taskItem = $this->TaskItem->find('all', array(
                'order' => array('priority' => 'DESC', 'creation_date' => 'ASC')
            )); 
        }

        $this->set([
            'taskItems' => $taskItem,
            '_serialize' => ['taskItems']
        ]);
    }

    /**
    * Busca a tarefa através do seu id
    * @param integer $id - id da tarefa
    * @return object - a tarefa especificada pelo $id
    */
    public function view($id = null)
    {
        $taskItem = $this->TaskItem->get($id, [
            'contain' => []
        ]);

        $this->set('taskItem', $taskItem);
        $this->set('_serialize', ['taskItem']);
    }

    /**
    * Persiste uma nova tarefa baseada nos dados recebidos na requisição
    * @return object - a tarefa criada
    */
    public function add()
    {
        $data = $this->request->getJsonInputAsArray();
        $taskItem = $this->TaskItem->newEntity($data);
        if ($this->TaskItem->save($taskItem)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            'taskItem' => $taskItem,
            '_serialize' => ['message', 'recipe']
        ]);
    }

    /**
    * Edita uma tarefa já existente, se baseando nos dados da requisição
    * @param integer $id - id da tarefa sendo editada
    * @return object - a tarefa já atualizada
    */
    public function edit($id)
    {
        $taskItem = $this->TaskItem->get($id);
        
        if ($this->request->is(['post', 'put'])) {
            
            $data = $this->request->getJsonInputAsArray();
            
            //ignora datas do front
            unset($data['creation_date']);
            unset($data['update_date']);
            unset($data['conclusion_date']);
            unset($data['removal_date']);

            $taskItem = $this->TaskItem->patchEntity($taskItem, $data);

            if($taskItem->id_status == 2 && empty($taskItem->conclusion_date)){
                 $taskItem->conclusion_date = Time::now();
            
            }else if($taskItem->id_status == 1 && !empty($taskItem->conclusion_date)){
                $taskItem->conclusion_date = null;

            }else{
                $taskItem->update_date = Time::now();
            }

            if ($this->TaskItem->save($taskItem)) {
                $message = 'Saved';
            } else {
                $message = $taskItem->errors();
            }
        }
        
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }    

    /**
    * Edita uma tarefa já existente, se baseando nos dados da requisição
    * @param integer $id - id da tarefa sendo editada
    * @return string - mesagem de sucesso ou falha
    */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        
        $taskItem = $this->TaskItem->get($id);
        
        $taskItem->id_status = 3;

        $taskItem->removal_date = Time::now();            

        if ($this->TaskItem->save($taskItem)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }
}
