<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TaskItem Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Cake\I18n\Time $creation_date
 * @property \Cake\I18n\Time $update_date
 * @property \Cake\I18n\Time $conclusion_date
 * @property \Cake\I18n\Time $removal_date
 * @property int $priority
 * @property int $id_status
 */
class TaskItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
