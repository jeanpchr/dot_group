<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TaskItem Model
 *
 * @method \App\Model\Entity\TaskItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\TaskItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TaskItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TaskItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaskItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TaskItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TaskItem findOrCreate($search, callable $callback = null)
 */
class TaskItemTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('task_item');
        $this->displayField('title');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('description');

        $validator
            ->dateTime('creation_date')
            ->allowEmpty('creation_date');

        $validator
            ->dateTime('update_date')
            ->allowEmpty('update_date');

        $validator
            ->dateTime('conclusion_date')
            ->allowEmpty('conclusion_date');

        $validator
            ->dateTime('removal_date')
            ->allowEmpty('removal_date');

        $validator
            ->integer('priority')
            ->notEmpty('priority');

        $validator
            ->integer('id_status')
            ->notEmpty('id_status');

        return $validator;
    }
}
