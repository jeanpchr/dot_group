<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaskItemTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaskItemTable Test Case
 */
class TaskItemTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TaskItemTable
     */
    public $TaskItem;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.task_item'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TaskItem') ? [] : ['className' => 'App\Model\Table\TaskItemTable'];
        $this->TaskItem = TableRegistry::get('TaskItem', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaskItem);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
