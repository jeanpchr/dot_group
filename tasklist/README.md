# Projeto tasklist (WS REST)

Esse projeto é um WS para o cadastro e gerenciamento de uma lista de atividades. 
Junto a esse WS também foi desenvolvido um cliente web para o mesmo. Ambos foram desenvolvidos como parte do processo seletivo da empresa Dot Group, organizado pela Supero TI.
Com esse software é possível:

  - Cadastras tarefas com título e descrição
  - Editar uma tarefa já cadastrada
  - Listar tarefas pendentes e concluídas
  - Apagar tarefas as quais o usuário não deseja mais visualizar

## Tecnologias

Esse WS foi desenvolvido com as seguinte tecnologias:
 
 - PHP
 - CakePHP
 - MySql
 - Composer

## Instalação
Para instalar o WS:
  
  - Copiar a pasta preferencialmente junto com o resto do projeto para o folder www de um servidor Apache com suporte a PHP.
  - Rode o script "tasklist_bd.sql" em um banco MySQL usando um usuário com permissões para criar bancos e outros usuários.

*Caso você deseje utilizar um usuário já existente no MySQL, é necessário modificar o datasource default no arquivo config\app.php (a partir da linha 221).

**Devido a essa aplicação ser construída em CakePHP, algumas condições são necessárias para seu funcionamento correto:
 
 - Habilitar o módulo "mod_rewrite" no servidor Apache.
 - PHP na versão 5.5.9 ou superior.
 - Possuir as extensões "mbstring" e "intl" habilitadas no PHP.