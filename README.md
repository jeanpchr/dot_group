# Processo Seletivo Supero TI - Dot Group 
Esse projeto contém a resolução das quatro questões do "Teste de Conhecimentos - Analista Desenvolvedor"*, que é parte do processo seletivo da empresa Dot Group. O projeto possui cinco pastas principais, cada uma delas contém o seguinte:

 - fizzbuzz: resolução da questão número 1 (Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos de ambos (3 e 5), imprima “FizzBuzz”)
 
 - session_cookie: resolução da questão número 2 (Refatore o código abaixo, fazendo as alterações que julgar necessário)

 - db_connection: resolução da questão número 3 (Refatore o código abaixo, fazendo as alterações que julgar necessário)

 - tasklist: contém o backend (ws rest) que é parte da resolução da questão número 4 (Desenvolva uma API Rest para um sistema gerenciador de tarefas ... As tarefas consistem em título e descrição, ordenadas por prioridade)

 - my_tasks: contém o frontend (client web) que é parte da resolução da questão número 4

##Instalação
A instalação dos sistemas contidos em fizzbuzz, session_cookie e db_connection é muito simples, basta copiar as pastas ou o projeto dot_group inteiro para o folder web de um servidor com suporte a PHP. Dentro de cada uma delas vai existir um arquivo index.php (deve ser visualizado através do browser), que oferece uma descrição resumida do que foi feito, além de outros arquivos que contém o código desenvolvido para resolver o problema em questão.

Os sistemas tasklist e my_tasks possuem mais detalhes de instalação e de uso, sendo assim cada um contém o seu próprio README.md, clique na pasta dos projeto para visualizá-los. 

*O pdf com o teste completo está disponível [aqui](https://drive.google.com/file/d/0B66fFiBrQWhNcERxd05WQkJFcjB1RmpzOHluRy1ueGpQY3dB/view?usp=sharing).