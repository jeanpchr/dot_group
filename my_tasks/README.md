# Projeto my_tasks (Cliente Web)

Esse projeto é um cliente web para o WS tasklist. 
Tanto esse cliente como o WS foram desenvolvidos como parte do processo seletivo
da empresa Dot Group, organizado pela Supero TI.
Com esse software é possível:

 - Cadastras tarefas com título e descrição
 - Editar uma tarefa já cadastrada
 - Listar tarefas pendentes e concluídas
 - Apagar tarefas as quais o usuário não deseja mais visualizar

## Tecnologias

Esse cliente web foi desenvolvido com as seguinte tecnologias:
 
 - AngularJS
 - Bootstrap
 - JQuery
 - HTML5
 - CSS3

## Instalação
A instalação do cliente web é similar a dos outros sistemas, basta copiar a pasta ou o projeto dot_group inteiro para o folder web de um servidor com suporte a PHP.

Entretanto, caso o ws seja instalado em um diretório diferente de "...\dot_group\tasklist", é necessário alterar a linha 9 do arquivo js\app.js para que a urlBase utilizada pelo cliente seja igual a urlBase do WS.