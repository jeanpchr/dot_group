var app = angular.module("tasksApp", []);

app.controller('TasksController', function($http) {

	/**
	* URL base para as requisições dese cliente, é necessário alterá-la caso o usuário instale
	* o WS em uma localização diferente da aponta abaixo.
	*/
	var baseUrl = 'http://localhost/dot_group/tasklist';

	/**
	* Lista de tarefas não concluídas	
	*/
	this.pendingTasks = [];

	/**
	* Lista de tarefas concluídas/finalizadas
	*/
	this.completeTasks = [];

	/**
	* Booleano que determina se o formulário de nova tarefa deve ser exibido no html	
	*/
	this.showNewTaskForm = false;

	/**
	* Boolean que determina se o formulário de edição de tarefa deve ser exibido no html.
	* Deve ser alterado apenas pelos métodos de edição	
	*/
	this.showEditTaskForm = false;
	
	/**
	* Wrapper para os dados de uma tarefa em edição	
	*/
	this.taskEdit = {};

	/**
	* String que representa a aba sendo visualizada pelo usuário	
	*/
	this.taskTab = 'pendentes';

	/**
	* Boolean para informar se as listas de tarefa precisam serem atualizadas após
	* trocar de aba
	*/
	this.refreshTab = true;

	/**
	* Possíveis status de uma atividade, lista utilizada internamente para algumas operações de atualização	
	*/
	this.statuses = {
		"pending" : 1,
		"finished" : 2,
		"removed" : 3
	}

	/**
	* Wrapper para os dados do formulário de nova tarefa	
	*/
	this.newTask = {"id_status" : this.statuses.pending, "priority" : 0};


	/**
	* Troca a aba sendo exibida
	* @param {String} tab - string com o identificador da aba que deve ser exibida
	*/
	this.setTab = function (tab) {
		
		if(this.refreshTab)	 {
			
			if(tab === 'pendentes') {
				this.loadPendingTasks();
				this.refreshTab = false;
			
			}else if(tab === 'concluidas') {
				this.loadFinishedTasks();
				this.refreshTab = false;
			}
		}
		
		this.taskTab = tab;
	}

	/**
	* Prepara e exibe o formulário de edição de tarefa
	* @param {Object} tab - tarefa a ser editada
	*/
	this.editTask = function(task) {
		this.taskEdit = task;
		this.showEditTaskForm = true;
	}

	/**
	* Envia uma requisição para atualizar a tarefa armazenada no atributo taskEdit
	*/
	this.updateTask = function() {
		$http({
			method: 'PUT',
			url: baseUrl + '/task_item/' + this.taskEdit.id + '.json',
			data : this.taskEdit,
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(
			(response)  => {
		    	if(this.taskTab === 'pendentes') {
					this.loadPendingTasks();
				}else if(this.taskTab === 'concluidas') {
					this.loadFinishedTasks();
				}

			}, (response)  => {
		    	alert("Falha no sistema");
			}
		);

		this.resetHideEditTaskForm();
	}

	/**
	* Limpa e esconde o formulário de edição  de tarefas
	*/
	this.resetHideEditTaskForm = function() {
		this.showEditTaskForm = false;
		this.taskEdit = {};
	}

	/**
	* Atualiza o status de uma tarefa pendente para finalizada/completa
	* @param {Object} tab - tarefa a ser finalizada
	*/
	this.finalizeTask = function(task) {
		task.id_status = this.statuses.finished;
		this.taskEdit = task;
		this.updateTask();
		this.refreshTab = true;
	}

	/**
	* Reverte o status de uma tarefa finalizada/completa para pendente
	* @param {Object} tab - tarefa a ter o status revertido
	*/
	this.setAsPending = function(task) {
		task.id_status = this.statuses.pending;
		this.taskEdit = task;
		this.updateTask();		
		this.refreshTab = true;
	}

	/**
	* Remove uma tarefa do usuário (indenpendente do seu status)
	* @param {Object} tab - tarefa a ser deletada
	*/
	this.deleteTask = function(task) {
		$http({
			method: 'DELETE',
			url: baseUrl + '/task_item/' + task.id + '.json',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(
			(response)  => {
		    	
		    	if(this.taskTab === 'pendentes') {
		    		this.loadPendingTasks();
		    	}else{
		    		this.loadFinishedTasks();
		    	}

			}, (response)  => {
		    	alert("Falha no sistema");
			}
		);
	}

	/**
	* Adiciona uma nova tarefa com base nos dados armazenados no atributo "newTask"
	*/
	this.addTask = function() {
		$http({
			method: 'POST',
			url: baseUrl + '/task_item.json',
			data : this.newTask,
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(
			(response)  => {
		    	this.loadPendingTasks();

			}, (response)  => {
		    	alert("Falha no sistema");
			}
		);

		this.resetHideNewTaskForm();
	}

	/**
	* Limpa e esconde o formulário de criação de tarefas
	*/
	this.resetHideNewTaskForm = function() {
		this.showNewTaskForm = false;
		this.newTask = {"id_status" : this.statuses.pending, "priority" : 0};
	}

	/**
	* Carrega a lista de tarefas pendentes
	*/
	this.loadPendingTasks = function() {
		$http({
			method: 'GET',
		 	url: baseUrl + '/task_item.json?status=1'		
		}).then(
			(response)  => {
		    	this.pendingTasks = response.data.taskItems;

			}, (response)  => {
		    	alert("Falha no sistema");
			}
		);
	}
	
	/**
	* Carrega a lista de tarefas concluídas/finalizadas
	*/
	this.loadFinishedTasks = function() {
		$http({
			method: 'GET',
		 	url: baseUrl + '/task_item.json?status=2'		
		}).then(
			(response)  => {
		    	this.completeTasks = response.data.taskItems;

			}, (response)  => {
		    	alert("Falha no sistema");
			}
		);
	}

	//inicializa a lista de tarefas pendentes (aba inicial ao carregar a página)
	this.loadPendingTasks();
});

app.directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function(val) {
                return '' + val;
            });
        }
    };
});